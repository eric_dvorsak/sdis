<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"  %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="">
	    <meta name="author" content="">
	
	    <title>SDIS Project</title>
	
	    <!-- Bootstrap core CSS -->
    	<link href="/SDIS_Project/bootstrap/css/bootstrap.css" rel="stylesheet">
	</head>
	<body>
		<div class="container-fluid">
			<div class="col-md-4" id="leftCol" style="width : 228px" >
				<h1>List of tasks to be done:</h1>
				<c:forEach items="${tasks}" var="task">
					<p>${task.toString()}</p>
				</c:forEach>
			</div><!--/left-->
			<div class="col-md-4">
				<form class="" method="post">
					<div class="form-group">
						<label>Enter a name :</label>
						<input class="form-control" type="text" name="name" required/>
					</div>
					<div class="form-group">
						<label>Enter a positive long number which represents the time that your task need to be done in s:</label>
						<input class="form-control" type="text" pattern="[0-9]+" name="charge" title="positive long number" required/>
					</div>
					<div class="form-group">
						<input class="btn btn-success" type="submit" value="envoyer la t�che">
					</div>
				</form>
			</div>
			<div class="col-md-4" id="rightCol" >
				<h1>Charge:</h1>
				<p>${charge}</p>
			</div>
			<div class="col-md-4" id="rightCol" >
				<h1>Number of workers:</h1>
				<p>${numberOfWorkers}</p>
			</div>
		</div>
		
		<!-- Bootstrap core JavaScript
	    ================================================== -->
	    <!-- Placed at the end of the document so the pages load faster -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	    <script src="/dalim_project/bootstrap/js/bootstrap.js"></script>
		</body>
</html>