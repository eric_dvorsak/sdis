package server;
import static java.lang.Math.abs;
import java.sql.Timestamp;

public class Tache {
	//this time that the task need to be done in ms
	private String name;
	private long charge;
        private Timestamp timestamp;
	
	public Tache(String name,long charge){
                java.util.Date date= new java.util.Date();
                this.timestamp= new Timestamp(date.getTime());
		this.name=name;
		this.charge=charge;
	}
	
	public long getCharge(){
		return this.charge;
	}
        
        public long getElapsedTime(){
                java.util.Date date= new java.util.Date();
                Timestamp time= new Timestamp(date.getTime());
                return time.getTime() - this.timestamp.getTime();            
        }
	
	public String toString() {
		return this.name+" needs "+this.charge+" ms to be done";
	}
}
