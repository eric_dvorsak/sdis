package server;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class Manager implements Observer, Runnable{
    private RabbitQueu queu;
    private List<Worker> workers;
    private int borne_sup;
    private long maxi_qds;
    private long mini_qds;

    public Manager(RabbitQueu queu,int borne_sup, int maxi_qds, int mini_qds){
        this.queu=queu;
        this.queu.addObserver(this);
        this.workers=new ArrayList<Worker>();
        this.borne_sup=borne_sup;
        this.maxi_qds = (long) maxi_qds;
        this.mini_qds = (long) mini_qds;
    }

    public int workerNumber(){
        return workers.size();
    }

    private void addWorker(){
        Worker newWorker=new Worker(this.queu);
        newWorker.addObserver(this);
        Thread t=new Thread(newWorker);
        this.workers.add(newWorker);
        t.start();
        System.out.println("Nouveau worker démarré");
    }

    private void removeWorker(){
        if(this.workers.size()>1){
            Worker worker = this.workers.get(0);
            if (worker.kill()) {
                System.out.println("Un worker a été ordonné de s'arrêter");
            }
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        if(o instanceof RabbitQueu) {
            System.out.println("Une nouvelle tâche a été ajoutée !");
            if (this.workers.size()==0) {
                this.addWorker();
            }
        }		
        if(o instanceof Worker) {
            this.workers.remove(o);
            System.out.println("Un worker a été arrêté");
        }
    };
    
    public void checkCharge() {
        while (true) {
            if (this.queu.getCharge() + this.mini_qds > this.maxi_qds 
                    || this.queu.size()>borne_sup) {
                this.addWorker();
                this.checkCharge();
            }
            else if (this.queu.getCharge() < this.mini_qds){
                this.removeWorker();
            }
            try {
                Thread.sleep((this.mini_qds / 2));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    
    @Override
    public void run() {
        this.checkCharge();
    }
}
	
