package server;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class RabbitQueu extends Observable{
	private List<Tache> queuTache;
        private long charge;
	
	public RabbitQueu(){
		this.queuTache=new ArrayList<Tache>();
                this.charge=0;
	}
	
        public long getCharge(){
            if (!this.isEmpty()) {
                this.charge= this.queuTache.get(0).getElapsedTime();
            }
            else {
                this.charge=0;
            }
            return this.charge;
        }
        
	public void add(Tache newTask){
		synchronized(this.queuTache){
			this.queuTache.add(newTask);
			this.setChanged();
			this.notifyObservers();
		}
	}
	
	public Tache consume(){
		synchronized(this.queuTache){
			Tache taskToConsume=this.queuTache.remove(0);
			return taskToConsume;
		}
	}
	
	public boolean isEmpty() {
		synchronized(this.queuTache){
			return this.queuTache.size()==0;
		}
	}
	
	public int size(){
		synchronized(this.queuTache){
			return this.queuTache.size();
		}
	}
	
	public List<Tache> getTache() {
		List<Tache> taches;
		synchronized (queuTache) {
			taches=new ArrayList<Tache>(queuTache);
		}
		return taches;
	}
}
