package server;

import java.util.Observable;
import java.sql.Timestamp;


public class Worker extends Observable implements Runnable{
	private RabbitQueu queu;
        private boolean alive;
	
	public Worker(RabbitQueu queu){
		this.queu=queu;
                this.alive = true;
	}
	
	public void work(){
		//the worker consume task until the queu is empty
		while(!queu.isEmpty() && this.alive){
			try {
				Tache taskToDo=queu.consume();
				Thread.sleep(taskToDo.getCharge());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		//once it is empty it notify the manager to get kick from the "alive" worker list and be considered dead
		this.setChanged();
		this.notifyObservers();
	}

        public boolean kill() {
            boolean state = this.alive;
            this.alive = false;
            return state;
        }
        
	@Override
	public void run() {
		this.work();
	}
}
