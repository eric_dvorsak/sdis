package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import server.Manager;
import server.RabbitQueu;
import server.Tache;

/**
 * Servlet implementation class HomeServlet
 */
@WebServlet("/")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private RabbitQueu queu=new RabbitQueu();
	private Manager manager=new server.Manager(queu, 500, 5000, 1000);
        private Thread thread=new Thread(this.manager);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HomeServlet() {
        super();
        this.thread.start();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("tasks", queu.getTache());
		request.setAttribute("numberOfWorkers", manager.workerNumber());
                request.setAttribute("charge", queu.getCharge());
		this.getServletContext().getRequestDispatcher("/WEB-INF/home.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String chargeString=request.getParameter("charge");
		String name=request.getParameter("name");
		// we are sure that charge represents a long thanks to the pattern attribute
		long charge= 1000 * Long.parseUnsignedLong(chargeString);
		this.queu.add(new Tache(name,charge));
		response.sendRedirect("");
	}

}
